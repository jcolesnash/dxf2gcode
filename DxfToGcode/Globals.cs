﻿using System.Windows.Media;

namespace DxfToGcode
{
    public static class Globals
    {
        //Screen info
        public static int ScreenDpi = 96;

        //Helpers (pixels)
        public static int InchMarkLength = 10;
        public static int EightInchMarkLength = 5;
        public static int MainAxisXLength = 24;
        public static int MainAxisYLength = 5;

        //Thickness
        public static int RoughingPassThickness = 2;
        public static int FinalPassThickness = 2;
        public static int PartThickness = 2;
        public static int InchMarkThickness = 2;
        public static int EightInchMarkThickness = 1;
        public static int MainAxisThickness = 2;

        //Colors
        public static Brush InchMarkColor = Brushes.Blue;
        public static Brush EightInchMarkColor = Brushes.Red;
        public static Brush MainAxisColor = Brushes.Blue;
        public static Brush RotationalAxisColor = Brushes.Pink;
        public static Brush RoughingPassColor = Brushes.Red;
        public static Brush PartColor = Brushes.Black;
        public static Brush SafeZoneColor = Brushes.DeepPink;
        public static Brush StockFill = Brushes.Bisque;

        //Options
        public static bool ShowSafeZone = false;
        public static bool ShowPassText = false;
        public static bool CutLeftSide = false;

        public static int ArcPrecision = 64;
        public static double CenterOffset = 250;
    }
}
