﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Shapes;

namespace DxfToGcode
{
	public class Roughing
	{
		public List<LatheCommand> CalculateRoughingPasses(Polyline finalPart, RoughingSettings roughingSettings)
		{
			var passList = new List<LatheCommand>();
			var safetyLine = GenerateSafeShape(finalPart, roughingSettings.SafeDistance);
			double firstIntersectionY = -1;

			for (var y = roughingSettings.StartY; y > roughingSettings.EndY; y -= roughingSettings.CuttingDepth)
			{
				var previousY = y + roughingSettings.CuttingDepth;
				var fullPath = new Line
				{
					X1 = roughingSettings.EndX,
					Y1 = y,
					X2 = roughingSettings.StartX,
					Y2 = y
				};

				//Move to start
				passList.Add(
					new LatheCommand(
						Gcodes.G00,
						roughingSettings.StartX,
						previousY + roughingSettings.CuttingDepth/2));

				//Feed In
				if (roughingSettings.RapidInfeed)
				{
					passList.Add(
						new LatheCommand(
							Gcodes.G00,
							roughingSettings.StartX,
							y));
				}
				else
				{
					passList.Add(
						new LatheCommand(
							Gcodes.G01,
							roughingSettings.StartX,
							y));
				}

				var intersectionPoints = safetyLine.FindIntersections(fullPath);
				
				if (intersectionPoints.Count > 0)
				{
					var rightMostIntersection = intersectionPoints.RightMost().X;
					var leftMostIntersection = intersectionPoints.LeftMost().X;
					if (firstIntersectionY == -1)
						firstIntersectionY = intersectionPoints[0].Y;

					var intersectionRapidY = firstIntersectionY + roughingSettings.CuttingDepth/2;

					passList.Add(
						new LatheCommand(
							Gcodes.G01,
							new Point(rightMostIntersection, y)));

					if (Globals.CutLeftSide && leftMostIntersection > 0 && intersectionPoints.Count > 1)
					{
						//rapid up
						passList.Add(
							new LatheCommand(
								Gcodes.G00,
								rightMostIntersection,
								intersectionRapidY));

						//rapid over to other side
						passList.Add(
							new LatheCommand(
								Gcodes.G01,
								leftMostIntersection,
								intersectionRapidY));


						//Feed In
						if (roughingSettings.RapidInfeed)
						{
							passList.Add(
								new LatheCommand(
									Gcodes.G00,
									leftMostIntersection,
									y));
						}
						else
						{
							passList.Add(
								new LatheCommand(
									Gcodes.G01,
									leftMostIntersection,
									y));
						}

						//cut to end
						passList.Add(
							new LatheCommand(
								Gcodes.G01,
								roughingSettings.EndX,
								y));

						//Go back to rapid Y
						passList.Add(
							new LatheCommand(
								Gcodes.G00,
								roughingSettings.EndX,
								intersectionRapidY));

						//Go back to rapid X
						passList.Add(
							new LatheCommand(
								Gcodes.G00,
								roughingSettings.StartX,
								intersectionRapidY));
					}
					else
					{
						//Go back to rapid
						passList.Add(
							new LatheCommand(
								Gcodes.G00,
								rightMostIntersection,
								previousY - roughingSettings.CuttingDepth/2));
					}
				}
				else
				{
					passList.Add(
						new LatheCommand(
							Gcodes.G01,
							roughingSettings.EndX,
							y));

					//Go back to rapid
					passList.Add(
						new LatheCommand(
							Gcodes.G00,
							roughingSettings.EndX,
							previousY - roughingSettings.CuttingDepth / 2));
				}
			}

			return passList;
		}

		public Polyline GenerateSafeShape(Polyline finalPart, double safetyMargin)
		{
			var polyOut = new Polyline();

			var average = finalPart.Points.Center();

			foreach (var point in finalPart.Points)
			{
				var innerSafetyMargin = safetyMargin;

				if (point.X < average.X)
					innerSafetyMargin = -safetyMargin;

				if (point == finalPart.Points.First() || point == finalPart.Points.Last())
				{
					polyOut.Points.Add(point.Y > 0 ? new Point(0, point.Y + safetyMargin) : new Point(point.X + innerSafetyMargin, 0));

					continue;
				}

				polyOut.Points.Add(new Point(point.X + innerSafetyMargin, point.Y + safetyMargin));
			}

			return polyOut;
		}
	}
}
