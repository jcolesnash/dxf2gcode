﻿namespace DxfToGcode
{
	public class RoughingSettings
	{
		public double StartX { get; set; }
		public double StartY { get; set; }
		public double EndX { get; set; }
		public double EndY { get; set; }
		public double SafeDistance { get; set; }
		public double CuttingDepth { get; set; }
		public bool RapidInfeed { get; set; }

		public RoughingSettings(double startX, double startY, double endX, double endY,
			double safeDistance, double cuttingDepth, bool rapidInfeed)
		{
			StartX = startX;
			StartY = startY;
			EndX = endX;
			EndY = endY;
			SafeDistance = safeDistance;
			CuttingDepth = cuttingDepth;
			RapidInfeed = rapidInfeed;
		}
	}
}
