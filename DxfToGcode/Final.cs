﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Shapes;

namespace DxfToGcode
{
    public class Final
    {
        public List<LatheCommand> CalculateFinalPass(Polyline finalPart, double startX, double startY, double endX, double endY)
        {
            var finalPoints = finalPart.ExplodeToLines().ExplodeToPoints();

            //If rightmost point is not first, flip
            var rightMost = finalPoints.RightMost();
            if (finalPoints.First() != rightMost)
                finalPoints.Reverse();

            //Trim to StartX
            var trimmedPoints = new List<Point>();
            foreach (var finalPoint in finalPoints)
            {
                if (finalPoint.X > endX)
                    trimmedPoints.Add(finalPoint);
                //else
                //{
                //terminate at endX, finalPoint.Y
                //}
            }

            var gCode = new List<LatheCommand>();

            gCode.Add(new LatheCommand(Gcodes.G00, string.Format("X{0}", startX)));
            gCode.Add(new LatheCommand(Gcodes.G01, startX, endY));
            gCode.AddRange(trimmedPoints.Select(finalPoint => new LatheCommand(Gcodes.G01, finalPoint)));

            return gCode;
        }
    }
}
