﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Win32;
using netDxf;

namespace DxfToGcode
{
	public partial class MainWindow
	{
		private Polyline _finalPart;
		private Polyline _safePart;

        private double _stockWidth;
        private double _stockDiameter;
        private double _cuttingDepth;
        private double _roughingDistance;
        private double _roughingStartX;
        private double _roughingStartY;
        private double _roughingEndX;
        private double _roughingEndY;
        private int _spindleSpeed;
        private int _feedRate;
        private bool _rapidInfeed;
	    private List<LatheCommand> _roughingPass = new List<LatheCommand>();
        private List<LatheCommand> _finalPass = new List<LatheCommand>();
        private List<string> _gCode = new List<string>(); 

        public MainWindow()
		{
			InitializeComponent();

			Globals.ScreenDpi = GetDpi();

			DrawHelpers();
		}

		private int GetDpi()
		{
			PresentationSource source = PresentationSource.FromVisual(this);

			double dpi = 96;
			if (source != null)
				if (source.CompositionTarget != null)
					dpi = 96.0 * source.CompositionTarget.TransformToDevice.M11;

			return (int)dpi;
		}

		#region Draw

		private void DrawStock(double stockWidth, double stockRadius)
		{
			var stock = new Rectangle
			{
				Width = stockWidth * Globals.ScreenDpi,
				Height = stockRadius * 2 * Globals.ScreenDpi,
				Fill = Globals.StockFill
			};

            Canvas.SetTop(stock, Globals.CenterOffset - stockRadius * Globals.ScreenDpi);
			ForeCanvas.Children.Add(stock);
		}

		private void DrawPart()
		{
			_finalPart.Stroke = Globals.PartColor;
			_finalPart.StrokeThickness = Globals.PartThickness;

			var polyline = _finalPart.Scale(Globals.ScreenDpi).Shift(0, 250);

			BackCanvas.Children.Add(polyline);
		}

	    private void DrawSafeZone()
	    {
            if (Globals.ShowSafeZone)
            {
                _safePart = new Roughing().GenerateSafeShape(_finalPart, double.Parse(TxtRoughingDistance.Text));

                _safePart.Stroke = Globals.SafeZoneColor;
                _safePart.StrokeThickness = Globals.PartThickness;

                var safeline = _safePart.Scale(Globals.ScreenDpi).Shift(0, 250);

                BackCanvas.Children.Add(safeline);
            }
        }

		private TextBlock Text(double x, double y, string text)
		{
			var textBlock = new TextBlock
			{
				Text = text,
				FontSize = 6,
				Foreground = new SolidColorBrush(Colors.Black)
			};

			Canvas.SetLeft(textBlock, x);
			Canvas.SetTop(textBlock, y);
			return textBlock;
		}

		private void DrawRoughingPass(IEnumerable<LatheCommand> roughingPasses)
		{
			var lastPoint = new Point(4, 2);

			var passNum = 0;
			foreach (var pass in roughingPasses)
			{
				ForeCanvas.Children.Add(new Line
				{
					X1 = lastPoint.X * Globals.ScreenDpi,
					X2 = pass.Target.X * Globals.ScreenDpi,
					Y1 = lastPoint.Y * Globals.ScreenDpi + Globals.CenterOffset,
					Y2 = pass.Target.Y * Globals.ScreenDpi + Globals.CenterOffset,
					StrokeThickness = Globals.RoughingPassThickness,
					Stroke = pass.Code == Gcodes.G00 ? Brushes.Black : Globals.RoughingPassColor
				});

				if (Globals.ShowPassText)
					ForeCanvas.Children.Add(Text(lastPoint.X * Globals.ScreenDpi + 8, lastPoint.Y * Globals.ScreenDpi + Globals.CenterOffset, passNum.ToString()));

				lastPoint = new Point(pass.Target.X, pass.Target.Y);
				passNum++;
			}
		}

        private void DrawHelpers()
        {
            DrawXaxis();
            DrawXaxisInchMarks();
            DrawXaxisEightMarks();
            DrawYaxis();
            DrawYaxisInchMarks();
            DrawYaxisEightMarks();
            DrawCenterLine();
        }

        private void DrawHelpers(double startX, double startY, double endX, double endY)
		{
			DrawHelpers();
            DrawEndLimits(startX, startY, endX, endY);
		}

		private void DrawYaxisInchMarks()
		{
			for (var y = Globals.ScreenDpi; y <= Globals.MainAxisYLength * Globals.ScreenDpi; y += Globals.ScreenDpi)
			{
				BackCanvas.Children.Add(new Line
				{
					Stroke = Globals.InchMarkColor,
					StrokeThickness = Globals.InchMarkThickness,
					X1 = 0,
					X2 = Globals.InchMarkLength,
					Y1 = y,
					Y2 = y
				});
			}
		}

		private void DrawYaxisEightMarks()
		{
			for (var y = 0; y <= Globals.MainAxisYLength * Globals.ScreenDpi; y += Globals.ScreenDpi / 8)
			{
				if (y % Globals.ScreenDpi == 0)
					continue;

				BackCanvas.Children.Add(new Line
				{
					Stroke = Globals.EightInchMarkColor,
					StrokeThickness = Globals.EightInchMarkThickness,
					X1 = 0,
					X2 = Globals.EightInchMarkLength,
					Y1 = y,
					Y2 = y
				});
			}
		}

		private void DrawXaxisInchMarks()
		{
			for (var x = Globals.ScreenDpi; x <= Globals.MainAxisXLength * Globals.ScreenDpi; x += Globals.ScreenDpi)
			{
				BackCanvas.Children.Add(new Line
				{
					Stroke = Globals.InchMarkColor,
					StrokeThickness = Globals.InchMarkThickness,
					X1 = x,
					X2 = x,
					Y1 = 0,
					Y2 = Globals.InchMarkLength
				});
			}
		}

		private void DrawXaxisEightMarks()
		{
			for (var i = 0; i <= Globals.MainAxisXLength * Globals.ScreenDpi; i += Globals.ScreenDpi / 8)
			{
				if (i % Globals.ScreenDpi == 0)
					continue;

				BackCanvas.Children.Add(new Line
				{
					Stroke = Globals.EightInchMarkColor,
					StrokeThickness = Globals.EightInchMarkThickness,
					X1 = i,
					X2 = i,
					Y1 = 0,
					Y2 = Globals.EightInchMarkLength
				});
			}
		}

		private void DrawXaxis()
		{
			BackCanvas.Children.Add(new Line
			{
				Stroke = Globals.MainAxisColor,
				StrokeThickness = Globals.MainAxisThickness,
				X1 = 0,
				X2 = Globals.MainAxisXLength * Globals.ScreenDpi,
				Y1 = 0,
				Y2 = 0
			});
		}

		private void DrawYaxis()
		{
			BackCanvas.Children.Add(new Line
			{
				Stroke = Globals.MainAxisColor,
				StrokeThickness = Globals.MainAxisThickness,
				X1 = 0,
				X2 = 0,
				Y1 = 0,
				Y2 = Globals.MainAxisYLength * Globals.ScreenDpi
			});
		}

	    private void DrawCenterLine()
	    {
            BackCanvas.Children.Add(new Line
            {
                Stroke = Globals.RotationalAxisColor,
                StrokeThickness = Globals.MainAxisThickness,
                X1 = 0,
                X2 = Globals.MainAxisXLength * Globals.ScreenDpi,
                Y1 = Globals.CenterOffset,
                Y2 = Globals.CenterOffset
            });
        }

        private void DrawEndLimits(double startX, double startY, double endX, double endY)
        {
            BackCanvas.Children.Add(new Line
            {
                Stroke = Globals.RotationalAxisColor,
                StrokeThickness = Globals.MainAxisThickness,
                X1 = startX * Globals.ScreenDpi,
                X2 = startX * Globals.ScreenDpi,
                Y1 = 0,
                Y2 = Globals.MainAxisXLength * Globals.ScreenDpi
            });

            BackCanvas.Children.Add(new Line
            {
                Stroke = Globals.RotationalAxisColor,
                StrokeThickness = Globals.MainAxisThickness,
                X1 = endX * Globals.ScreenDpi,
                X2 = endX * Globals.ScreenDpi,
                Y1 = 0,
                Y2 = Globals.MainAxisXLength * Globals.ScreenDpi
            });

            BackCanvas.Children.Add(new Line
            {
                Stroke = Globals.RotationalAxisColor,
                StrokeThickness = Globals.MainAxisThickness,
                X1 = 0,
                X2 = Globals.MainAxisYLength * Globals.ScreenDpi,
                Y1 = startY * Globals.ScreenDpi + Globals.CenterOffset,
                Y2 = startY * Globals.ScreenDpi + Globals.CenterOffset
            });

            BackCanvas.Children.Add(new Line
            {
                Stroke = Globals.RotationalAxisColor,
                StrokeThickness = Globals.MainAxisThickness,
                X1 = 0,
                X2 = Globals.MainAxisYLength * Globals.ScreenDpi,
                Y1 = endY * Globals.ScreenDpi + Globals.CenterOffset,
                Y2 = endY * Globals.ScreenDpi + Globals.CenterOffset
            });
        }

	    private void DrawEverything()
	    {
            ForeCanvas.Children.Clear();
            BackCanvas.Children.Clear();

            DrawPart();
            DrawSafeZone();
            DrawHelpers(_roughingStartX, _roughingStartY, _roughingEndX, _roughingEndY);
            DrawStock(_stockWidth, _stockDiameter / 2);
            DrawRoughingPass(_roughingPass);
        }

        #endregion

        #region FormActions
        private void BtnDrawClick(object sender, RoutedEventArgs e)
		{
			GetFormFields();

            var roughing = new Roughing();
			_roughingPass = roughing.CalculateRoughingPasses(_finalPart, new RoughingSettings(
				_roughingStartX,
				_roughingStartY,
				_roughingEndX,
				_roughingEndY,
				_roughingDistance,
				_cuttingDepth,
				_rapidInfeed));

            var final = new Final();
			_finalPass = final.CalculateFinalPass(_finalPart, _roughingStartX, _roughingStartY, _roughingEndX, _roughingEndY);
			_gCode = GenerateGcode();

			DrawEverything();
		}

	    private void GetFormFields()
	    {
	        _stockWidth = double.Parse(TxtStockWidth.Text);
	        _stockDiameter = double.Parse(TxtStockDiameter.Text);
	        _cuttingDepth = double.Parse(TxtCuttingDepth.Text);
	        _roughingDistance = double.Parse(TxtRoughingDistance.Text);
	        _roughingStartX = double.Parse(TxtRoughingStartX.Text);
	        _roughingStartY = double.Parse(TxtRoughingStartY.Text);
	        _roughingEndX = double.Parse(TxtRoughingEndX.Text);
	        _roughingEndY = double.Parse(TxtRoughingEndY.Text);
	        _spindleSpeed = int.Parse(TxtSpindleSpeed.Text);
	        _feedRate = int.Parse(TxtFeedrate.Text);

	        if (ChkRapidInfeed.IsChecked.HasValue)
	            _rapidInfeed = ChkRapidInfeed.IsChecked.Value;

	        if (ChkShowSafeZone.IsChecked.HasValue)
	            Globals.ShowSafeZone = ChkShowSafeZone.IsChecked.Value;

	        if (ChkShowPassNum.IsChecked.HasValue)
	            Globals.ShowPassText = ChkShowPassNum.IsChecked.Value;

	        if (ChkCutLeftSide.IsChecked.HasValue)
	            Globals.CutLeftSide = ChkCutLeftSide.IsChecked.Value;
	    }

	    private List<string> GenerateGcode()
		{
			var gCode = new List<string>();

			gCode.Add(new LatheCommand(Gcodes.S, _spindleSpeed).FullCode);
			gCode.Add(new LatheCommand(Gcodes.F, _feedRate).FullCode);

			gCode.Add(new LatheCommand("StartRoughing").FullCode);
			gCode.AddRange(_roughingPass.Select(pass => pass.FullCode));
			gCode.Add(new LatheCommand("EndRoughing").FullCode);
			gCode.Add(new LatheCommand("StartFinal").FullCode);
			gCode.AddRange(_finalPass.Select(pass => pass.FullCode));
			gCode.Add(new LatheCommand("EndFinal").FullCode);

			gCode.Add(new LatheCommand(Gcodes.M05).FullCode);
			gCode.Add(new LatheCommand(Gcodes.M30).FullCode);
            
		    return gCode;
		    //File.WriteAllLines(Environment.CurrentDirectory + "\\text.gcode", gCode.ToArray());
		}
        
		private void BtnBrowseClick(object sender, RoutedEventArgs e)
		{
			var dlg = new OpenFileDialog
			{
				InitialDirectory = Environment.CurrentDirectory,
				FileName = "file.dxf",
				DefaultExt = ".dxf",
				Filter = "Cad File (.dxf)|*.dxf"
			};
			var result = dlg.ShowDialog();

			if (result == true)
			{
				if (!File.Exists(dlg.FileName))
				{
					MessageBox.Show("File failed to load");
					return;
				}

				TxtFileName.Text = dlg.FileName;

                if (File.Exists(TxtFileName.Text))
                {
                    var loadedFile = DxfDocument.Load(TxtFileName.Text);

                    _finalPart = loadedFile.LwPolylines[0].Polyline();

                    DrawEverything();
                }
            }
		}

		#endregion

	    private void BtnSavegCodeClick(object sender, RoutedEventArgs e)
	    {
            var dlg = new SaveFileDialog
            {
                InitialDirectory = Environment.CurrentDirectory,
                FileName = "file.gcode",
                DefaultExt = ".gcode",
                Filter = "GCode (.gcode)|*.gcode|GCode(.nc)|*.nc"
            };

            var result = dlg.ShowDialog();

            if (result == true)
            {
                File.WriteAllLines(dlg.FileName, _gCode);
            }
        }
	}
}
