﻿namespace DxfToGcode
{
	public enum Gcodes
	{
		G01,
		G00,
		F,
		S,
		M05,
		M30,
		Comment
	}
}