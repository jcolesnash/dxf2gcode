﻿using System.Windows;

namespace DxfToGcode
{
	public class StartEnd
	{
		public Point Start { get; private set; }
		public Point End { get; private set; }

		public StartEnd(Point start, Point end)
		{
			Start = start;
			End = end;
		}
	}
}