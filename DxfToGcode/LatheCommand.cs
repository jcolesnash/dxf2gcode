﻿using System.Windows;

namespace DxfToGcode
{
	public class LatheCommand
	{
		public Gcodes Code { get; private set; }
		public string Details { get; private set; }
		public Point Target { get; private set; }
		public string FullCode { get; private set; }
		public int Speed { get; private set; }

		public LatheCommand(string comment)
		{
			Code = Gcodes.Comment;
			Details = comment;
			FullCode = string.Format("({0})", comment);
		}

		public LatheCommand(Gcodes code)
		{
			Code = code;
			FullCode = Code.ToString();
		}

		public LatheCommand(Gcodes code, string details)
		{
			Code = code;
			Details = details;
			FullCode = string.Format("{0} {1}", Code, Details);
		}

		public LatheCommand(Gcodes code, Point moveTo)
		{
			Code = code;
			Target = moveTo;
			Details = string.Format("X{0}Y{1}", moveTo.X, moveTo.Y);
			FullCode = string.Format("{0} {1}", Code, Details);
		}

		public LatheCommand(Gcodes code, double x, double y)
		{
			Code = code;
			Target = new Point(x,y);
			Details = string.Format("X{0}Y{1}", x, y);
			FullCode = string.Format("{0} {1}", Code, Details);
		}

		public LatheCommand(Gcodes code, int speed)
		{
			Code = code;
			Speed = speed;
			Details = speed.ToString();
			FullCode = string.Format("{0} {1}", Code, Details);
		}
	}
}