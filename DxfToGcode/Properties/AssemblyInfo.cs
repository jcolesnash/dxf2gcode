﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: AssemblyTitle("DxfToGcode")]
[assembly: AssemblyDescription("Dxf to G-Code Cam software")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("James Coles-Nash")]
[assembly: AssemblyProduct("DxfToGcode")]
[assembly: AssemblyCopyright("Copyright James Coles-Nash 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(
	ResourceDictionaryLocation.None,
	ResourceDictionaryLocation.SourceAssembly
)]
