﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows.Media;
using netDxf.Entities;
using Line = System.Windows.Shapes.Line;
using Point = System.Windows.Point;
using Polyline = System.Windows.Shapes.Polyline;

namespace DxfToGcode
{
	public static class Extensions
	{
		public static List<Line> ExplodeToLines(this Polyline poly)
		{
			var expandedPolyLines = new List<Line>();

			var lastPoint = poly.Points[0];
			for (int p = 1; p < poly.Points.Count; p++)
			{
				var currentPoint = new Point(poly.Points[p].X, poly.Points[p].Y);
				var tempLine = new Line
				{
					X1 = lastPoint.X,
					Y1 = lastPoint.Y,
					X2 = currentPoint.X,
					Y2 = currentPoint.Y
				};

				expandedPolyLines.Add(tempLine);
				lastPoint = currentPoint;
			}

			return expandedPolyLines;
		}

		public static List<Point> ExplodeToPoints(this List<Line> lines)
		{
			var points = new List<Point>();

			foreach (var line in lines)
			{
				points.Add(new Point(line.X1, line.Y1));
				points.Add(new Point(line.X2, line.Y2));
			}

			return points;
		}

		public static Point Center(this PointCollection points)
		{
			return new Point(points.Average(e => e.X), points.Average(e => e.Y));
		}

		public static Point RightMost(this List<Point> points)
		{
			var rightMost = new Point();
			foreach (var point in points)
			{
				if (point.X > rightMost.X)
					rightMost = point;
			}

			return rightMost;
		}

	    public static Point RightMost(this LwPolyline polyline)
	    {
	        var rightMost = new Point();
	        foreach (var vertex in polyline.Vertexes)
	        {
	            if (vertex.Location.X > rightMost.X)
                    rightMost = new Point(vertex.Location.X, vertex.Location.Y);
	        }

	        return rightMost;
	    }

		public static Point LeftMost(this List<Point> points)
		{
			var leftMost = new Point(points.RightMost().X,0);
			foreach (var point in points)
			{
				if (point.X < leftMost.X)
					leftMost = point;
			}

			return leftMost;
		}

        public static Point LeftMost(this LwPolyline polyline)
        {
            var leftMost = new Point();
            foreach (var vertex in polyline.Vertexes)
            {
                if (vertex.Location.X < leftMost.X)
                    leftMost = new Point(vertex.Location.X, vertex.Location.Y);
            }

            return leftMost;
        }

        public static Point BottomMost(this List<Point> points)
		{
			var bottomMost = new Point();
			foreach (var point in points)
			{
				if (point.Y > bottomMost.Y)
					bottomMost = point;
			}

			return bottomMost;
		}

		public static List<Point> FindIntersections(this Polyline finalPart, Line fullPath)
		{
			var intersectionPoints = new List<Point>();

			foreach (var line in finalPart.ExplodeToLines())
			{
				var intersectionPoint = line.FindIntersection(fullPath);

				if (intersectionPoint == null)
					continue;

				intersectionPoints.Add((Point)intersectionPoint);
			}

			return intersectionPoints;
		}

		public static Point? FindIntersection(this Line lineA, Line lineB)
		{
			return FindIntersection(new Point(lineA. X1, lineA.Y1),
				new Point(lineA.X2, lineA.Y2),
				new Point(lineB.X1, lineB.Y1),
				new Point(lineB.X2, lineB.Y2));
		}

		public static Point? FindIntersection(Point p1, Point p2, Point p3, Point p4)
		{
			var v1 = new Point
			{
				X = p2.X - p1.X, 
				Y = p2.Y - p1.Y
			};

			var v2 = new Point
			{
				X = p4.X - p3.X, 
				Y = p4.Y - p3.Y
			};

			var d = v1.X * v2.Y - v1.Y * v2.X;
			if (d.Equals(0))
			{
				return null;
			}

			var a = p3.X - p1.X;
			var b = p3.Y - p1.Y;
			var t = (a * v2.Y - b * v2.X) / d;
			var s = (b * v1.X - a * v1.Y) / -d;
			if (t < 0 || t > 1 || s < 0 || s > 1)
			{
				return null;
			}

			var intersectPoint = new Point
			{
				X = p1.X + v1.X*t, 
				Y = p1.Y + v1.Y*t
			};
			return intersectPoint;
		}

		public static double Det2(double x1, double x2, double y1, double y2)
		{
			return (x1 * y2 - y1 * x2);
		}

		public static Polyline Polyline(this LwPolyline lwPolyLine)
		{
			var polyLine = new Polyline
			{
				Stroke = Brushes.Black,
				StrokeThickness = 2
			};

			foreach (var lwPolylineVertex in lwPolyLine.Vertexes)
			{
				polyLine.Points.Add(new Point(lwPolylineVertex.Location.X, lwPolylineVertex.Location.Y));
			}

			return polyLine;
		}

		public static Polyline Scale(this Polyline polylineIn, double scale)
		{
			var polyLine = new Polyline
			{
				Stroke = polylineIn.Stroke,
				StrokeThickness = polylineIn.StrokeThickness
			};

			foreach (var point in polylineIn.Points)
			{
				polyLine.Points.Add(new Point(point.X * scale, point.Y * scale));
			}

			return polyLine;
		}

	    public static Polyline Shift(this Polyline polylineIn, int xShift, int yShift)
	    {
            var polyLine = new Polyline
            {
                Stroke = polylineIn.Stroke,
                StrokeThickness = polylineIn.StrokeThickness
            };

            foreach (var point in polylineIn.Points)
	        {
	            polyLine.Points.Add(new Point(point.X + xShift, point.Y + yShift));
	        }

            return polyLine;
	    }

		public static double Distance(this Point pointA, Point pointB)
		{
			var dX = pointA.X - pointB.X;
			var dY = pointA.Y - pointB.Y;
			var multi = dX * dX + dY * dY;
			return Math.Round(Math.Sqrt(multi), 3);
		}
	}
}
